var chart = circularHeatChart()
    .segmentHeight(20)
    .innerRadius(20)
    .numSegments(24)
    .radialLabels(["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"])
    .segmentLabels(["Midnight", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "Midday", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm"])
    .margin({top: 20, right: 0, bottom: 20, left: 20});



/* An array of objects */
data = [];
// for(var i=0; i<63; i++) {
//     data[i] = {title: "Attraction ", value: Math.floor(Math.random() * 10) + 1};
// }

// dummy data
data = [
    {title: "Dinosaur ", value: 5},{title: "Dinosaur ", value: 5},{title: "Dinosaur ", value: 5},{title: "Dinosaur ", value: 5},{title: "Dinosaur ", value: 15},{title: "Dinosaur ", value: 5},{title: "Dinosaur ", value: 5},{title: "Dinosaur ", value: 5},{title: "Dinosaur ", value: 5},
    {title: "Expedition Everest ", value: 15},{title: "Expedition Everest ", value: 15},{title: "Expedition Everest ", value: 15},{title: "Expedition Everest ", value: 15}, {title: "Expedition Everest ", value: 15},{title: "Expedition Everest ", value: 15},{title: "Expedition Everest ", value: 15},{title: "Expedition Everest ", value: 15},{title: "Expedition Everest ", value: 15},
    {title: "Kali River Rapids ", value: 5},{title: "Kali River Rapids ", value: 5},{title: "Kali River Rapids ", value: 5},{title: "Kali River Rapids ", value: 15}, {title: "Kali River Rapids ", value: 5},{title: "Kali River Rapids ", value: 5},{title: "Kali River Rapids ", value: 5},{title: "Kali River Rapids ", value: 5},{title: "Kali River Rapids ", value: 5},
    {title: "Kilimanjaro Safaris ", value: 10},{title: "Kilimanjaro Safaris ", value: 10},{title: "Kilimanjaro Safaris ", value: 10},{title: "Kilimanjaro Safaris ", value: 15}, {title: "Kilimanjaro Safaris ", value: 10},{title: "Kilimanjaro Safaris ", value: 10},{title: "Kilimanjaro Safaris ", value: 10},{title: "Kilimanjaro Safaris ", value: 10},{title: "Kilimanjaro Safaris ", value: 10},
    {title: "It's Tough To Be A Bug ", value: 5},{title: "It's Tough To Be A Bug ", value: 5},{title: "It's Tough To Be A Bug ", value: 5},{title: "It's Tough To Be A Bug ", value: 5}, {title: "It's Tough To Be A Bug ", value: 15},{title: "It's Tough To Be A Bug ", value: 5},{title: "It's Tough To Be A Bug ", value: 5},{title: "It's Tough To Be A Bug ", value: 5},{title: "It's Tough To Be A Bug ", value: 5},
    {title: "Na'Vi River Journey ", value: 100},{title: "Na'Vi River Journey ", value: 100},{title: "Na'Vi River Journey ", value: 100},{title: "Na'Vi River Journey ", value: 100}, {title: "Na'Vi River Journey ", value: 100},{title: "Na'Vi River Journey ", value: 100},{title: "Na'Vi River Journey ", value: 100},{title: "Na'Vi River Journey ", value: 100},{title: "Na'Vi River Journey ", value: 100},
    {title: "Avatar: Flight of Passage ", value: 20},{title: "Avatar: Flight of Passage ", value: 20},{title: "Avatar: Flight of Passage ", value: 20},{title: "Avatar: Flight of Passage ", value: 150}, {title: "Avatar: Flight of Passage ", value: 150},{title: "Avatar: Flight of Passage ", value: 150},{title: "Avatar: Flight of Passage ", value: 50},{title: "Avatar: Flight of Passage ", value: 50},{title: "Avatar: Flight of Passage ", value: 50}
]



chart.accessor(function(d) {return d.value;})
    .range(["white", "black"])
    .radialLabels(["Dinosaur", "Expedition Everest", "Kali River Rapids", "Kilimanjaro Safaris", "It's Tough To Be A Bug", "Na'Vi River Journey", "Avatar:Flight of Passage"])
    .segmentLabels(["9am", "10am", "11am","Midday", "1pm", "2pm", "3pm", "4pm", "5pm"])
    .numSegments(9) // this is the hours in the day
    .segmentHeight(45); // this is the hours * the number of attractions
d3.select('#chart4')
    .selectAll('svg')
    .data([data])
    .enter()
    .append('svg')
    .style('width', '1100px')
    .style('height', '1100px')
    .call(chart);

/* Add a mouseover event */
d3.selectAll("#chart4 path").on('mouseover', function() {
	var d = d3.select(this).data()[0];
    d3.select("#info").text(d.title + ' has a wait of ' + d.value + ' minutes');
});
d3.selectAll("#chart4 svg").on('mouseout', function() {
    d3.select("#info").text('');	
});
